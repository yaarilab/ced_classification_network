import pandas as pd
import numpy as np
import pickle
from numpy import argmax
import torch
from torch.utils.data import DataLoader, TensorDataset, WeightedRandomSampler
from sklearn.model_selection import train_test_split
 

#### functions #####
# read the vdj sequences table and filter the relavent columns.
# If the falg `enrich_positive_samples` get a positive number larger than 0,
# then the number of positive samples is duplicated to the size of the number.
# input: path to sequence table, enrich_positive_samples the number of repeats of the positive samples (Defualt is 0)
# output: data frame of the relavent columns
def read_csv(path, enrich_positive_samples=0):
    df = pd.read_csv(path, sep=",", usecols=["JUNC_AA", "TG2", "GLIADIN", "JUNCTION_LENGTH", "V_CALL", "D_CALL", "J_CALL"])
    df = df[df["TG2"] != 'Neutral']
    df['TG2'] = df['TG2'].replace(['Negative','Positive'],[0,1])
    df['GLIADIN'] = df['GLIADIN'].replace(['Negative','Positive'],[0,1])
    df['labels'] = df['TG2'] | df['GLIADIN'] # add a label column that indicate if a sequence is postive to TG2 or to GLIADIN

    if enrich_positive_samples>0:
        # repeat the positive samples n times.
        # print initial number of cases
        print('Initial number of cases: Negative ', str(df.labels.value_counts()[0]), " Positive ", str(df.labels.value_counts()[1]))
        sub_positive = df[df.labels == True]
        i = 1
        while i < enrich_positive_samples: # one less than the number.
            df = pd.concat([df,sub_positive], ignore_index=True)
            i+=1
        # print number of cases after enrichment
        print('After enrichment: Negative ', str(df.labels.value_counts()[0]), " Positive ", str(df.labels.value_counts()[1]))
    return df


# translate an amino acid (AA) sequence into a numeric array. 
# Each AA has its own unique numbering, going form 1 to 21.
# input: AA sequence
# output: numeric array
def encode(s1):
    a = []
    dic = {'0':0,'A':1,'C':2,'D':3,'E':4,'F':5,'G':6,'H':7,'I':8,'K':9,
           'L':10,'M':11,'N':12,'P':13,'Q':14,'R':15,'S':16,'T':17,
           'V':18,'W':19,'Y':20,'X':21,'!':22}
    for i in range(len(s1)):
        a.append(dic.get(s1[i]))
    return a

# creates two arrays which contains the encoded protein sequence and the label.
# input: data frame, sequence column, label column
# output: encoded sequence array, labels array
def TrainData(df, seq_column = "JUNC_AA", label_column = "labels"):
    seq_line = [encode(seq) for seq in df[seq_column].values]
    label_line = df[label_column].values
    return seq_line,label_line


#Data segmentation
def Data_division(str_path, nb_words=None, skip_top=0,
              maxlen=None, test_split=0.15, seed=113,
              start_char=1, oov_char=2, index_from=3):
    X,labels = pickle.load(open(str_path, "rb"))
    np.random.seed(seed)
    np.random.shuffle(X)
    np.random.seed(seed)
    np.random.shuffle(labels)
    if maxlen:
        new_X = []
        new_labels = []
        for x, y in zip(X, labels):
            if len(x) < maxlen:
                new_X.append(x)
                new_labels.append(y)
        X = new_X
        labels = new_labels
    if not X:
        raise Exception('After filtering for sequences shorter than maxlen=' +
                        str(maxlen) + ', no sequence was kept. '
                                      'Increase maxlen.')
    if not nb_words:
        nb_words = max([max(x) for x in X])
    X_train = np.array(X[:int(len(X) * (1 - test_split))], dtype=object)
    y_train = np.array(labels[:int(len(X) * (1 - test_split))], dtype='float32')
    X_test = np.array(X[int(len(X) * (1 - test_split)):], dtype=object)
    y_test = np.array(labels[int(len(X) * (1 - test_split)):], dtype='float32')
    return (X_train, y_train), (X_test, y_test)