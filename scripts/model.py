from __future__ import print_function
import numpy as np
import numpy as np
import pickle
import processing as pg
import os.path
import h5py
import sklearn  
import keras
from keras.models import model_from_json, load_model, Sequential, Model
from keras.preprocessing import sequence
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM, GRU, SimpleRNN
from keras.layers.convolutional import Convolution1D, MaxPooling1D
from keras.layers import Dense, LSTM, Lambda, TimeDistributed, Input, Masking, Bidirectional, Flatten
from keras.datasets import imdb
from keras.utils import np_utils
from keras.utils.vis_utils import plot_model
from keras.callbacks import History, ModelCheckpoint, LearningRateScheduler
from keras import backend as K
from matplotlib import pyplot as plt
plt.switch_backend('agg')
from tabulate import tabulate
from keras_balanced_batch_generator import make_generator
from tensorflow.keras.utils import to_categorical
from tensorflow.keras import optimizers
from imblearn.tensorflow import balanced_batch_generator
from imblearn.under_sampling import NearMiss
from imblearn.over_sampling import RandomOverSampler 
import platform
import argparse
from sklearn.utils import class_weight
from mlxtend.plotting import plot_confusion_matrix
from sklearn.model_selection import train_test_split
import os

###############################################
############# server run ######################
###############################################
# conda activate /home/bcrlab/peresay/.conda/envs/py39-

if platform.node().startswith("ig"):
    parser = argparse.ArgumentParser()
    parser.add_argument("--batch_size", help="The batch size to run. Default is 128",type=int, default = 128)
    parser.add_argument("--epochs", help="number of epochs. Defualt is 50",type=int, default = 50)
    parser.add_argument("--LR", help="learning rate size. Defualt is 0.001",type=np.float32, default = 0.001)
    parser.add_argument("--embedding_size", help="The size for the output of the emmbeding. Default is 32",type=int, default = 32)
    parser.add_argument("--run_name", help="The run name",type=str, default = "run1")
    parser.add_argument("--generator", help="If to balance the labels in each batch",action='store_true', default = False)
    parser.add_argument("--add_weights", help="If to balance the labels with weights",action='store_true', default = False)
    parser.add_argument("--optimizer", help="If to use an Adam optimizer or Time-based decay",action='store_true', default = False)
    parser.add_argument("--early_stopping", help="If to use an early stopping rule",action='store_true', default = False)
    parser.add_argument("--enrichment", help="If to enrich the data by a factor of 8",action='store_true', default = False)
    parser.add_argument("--model_type", help="If to run CNN/BiLSTM/CNN-BiLSTM",type=str, default = "CNN-BiLSTM")
    parser.add_argument("--split_threshold", help="The threshold of train-test split",type=np.float32, default = 0.2)

    args = parser.parse_args()
else:
    args = argparse.Namespace()
    args.epochs = 50
    args.batch_size = 128
    args.LR = 0.001
    args.run_name = "_run1"
    args.generator = False
    args.add_weights = False
    args.split_threshold = 0.2
   
###############################################
############# read and sort the data ##########
###############################################
if args.enrichment:
    pickle_file = "alldata8.pkl"
else:
    pickle_file = "alldata.pkl"

if not os.path.isfile(pickle_file):
    # read the data
    path = "reprocess_output_2.csv"
    data = pg.read_csv(path, enrich_positive_samples = 0) # 0/ 3/ 8

    # get encoded data
    a,b=pg.TrainData(data)
    t = (a, b)

    # save data as pickle
    pickle.dump(t,open("alldata.pkl","wb"))  #alldata8.pkl

###############################################
############# Initial parameters ##############
###############################################

# reproducibility
seed = 1258
np.random.seed(seed)

# Embedding
max_features = 23 # number of AA
maxlen_pad = 21
maxlen = maxlen_pad + 5 # max number of AA sequence in the celiac repertoire + 5 (78+5=83)
embedding_size = args.embedding_size

# Convolution
nb_filter = 32 
pool_length = 2

# BiLSTM
bilstm_output_size = 70

# TrainingSet
batch_size = args.batch_size # maybe this should be changed
epochs = args.epochs

### create run directory for output files

os.makedirs(args.run_name, exist_ok=True)
out_path = args.run_name+"/"

### write params to file

with open(out_path+'params.txt', 'a') as the_file:
    the_file.write('###############################################')
    the_file.write('############# Initial parameters ##############')
    the_file.write('###############################################')
    the_file.write('seed: '+ "1258")
    the_file.write('max_features: '+ "23")
    the_file.write('embedding_size: '+ str(embedding_size))
    the_file.write('nb_filter: '+ "32")
    the_file.write('pool_length: '+ "2")
    the_file.write('bilstm_output_size: '+ "70")
    the_file.write('batch_size: '+ str(batch_size))
    the_file.write('learning rate: '+ str(args.LR))
    the_file.write('droupout: '+ str(0.3))
    the_file.write('cnn kernel sizes: L1 - 10, L2 - 5')
    the_file.write('cnn activation: relu') # maybe try to run selu??
    the_file.write('generator: '+ str(args.generator))
    the_file.write('add_weights: '+ str(args.add_weights))
    the_file.write('model_type: '+ str(args.model_type))

###############################################
############# Devided data for model ##########
###############################################
print('Loading data...')
#(X_train, y_train), (X_test, y_test) = pg.Data_division("alldata.pkl",nb_words=max_features,
#                                                     test_split=args.split_threshold)

if args.enrichment:
    X,labels = pickle.load(open("alldata8.pkl", "rb"))    
else:
    X,labels = pickle.load(open("alldata.pkl", "rb"))    


X_train, X_test, y_train, y_test = train_test_split(X, labels, test_size=0.2, shuffle=True, random_state=113)  
X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.25, random_state=113)                                                     

print(len(X_train), 'train sequences')
unique, counts = np.unique(y_train, return_counts=True)
print('train sequences labels devision')
print(tabulate([[unique[0], counts[0], counts[0]/sum(counts)*100], [unique[1], counts[1], counts[1]/sum(counts)*100]], headers=['labels', 'count', 'percentage']))
print('############################################')
print(len(X_val), 'validation sequences')
unique, counts = np.unique(y_val, return_counts=True)
print('validation sequences labels devision')
print(tabulate([[unique[0], counts[0], counts[0]/sum(counts)*100], [unique[1], counts[1], counts[1]/sum(counts)*100]], headers=['labels', 'count', 'percentage']))
('############################################')
print(len(X_test), 'test sequences')
print('test sequences labels devision')
unique, counts = np.unique(y_test, return_counts=True)
print(tabulate([[unique[0], counts[0], counts[0]/sum(counts)*100], [unique[1], counts[1], counts[1]/sum(counts)*100]], headers=['labels', 'count', 'percentage']))
print('############################################')
print('Pad sequences (samples x time) and add stop sequence')

X_train = sequence.pad_sequences(sequence.pad_sequences(X_train, maxlen=maxlen_pad, padding="post"), maxlen=maxlen, padding="post", value = 22)
X_val = sequence.pad_sequences(sequence.pad_sequences(X_val, maxlen=maxlen_pad, padding="post"), maxlen=maxlen, padding="post", value = 22)
X_test = sequence.pad_sequences(sequence.pad_sequences(X_test, maxlen=maxlen_pad, padding="post"), maxlen=maxlen, padding="post", value = 22)
print('X_train shape:', X_train.shape)
print('X_val shape:', X_val.shape)
print('X_test shape:', X_test.shape)

###############################################
############# Build the model #################
###############################################

def recall_m(y_true, y_pred): # TPR
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1))) # TP
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1))) # P
    recall = true_positives / (possible_positives + K.epsilon())
    return recall

def precision_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1))) # TP
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1))) # TP + FP
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision

def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))

def TP(y_true, y_pred):
    tp = K.sum(K.round(K.clip(y_true * y_pred, 0, 1))) # TP
    y_pos = K.round(K.clip(y_true, 0, 1))
    n_pos = K.sum(y_pos)
    y_neg = 1 - y_pos
    n_neg = K.sum(y_neg)
    n = n_pos + n_neg
    return tp/n

def TN(y_true, y_pred):
    y_pos = K.round(K.clip(y_true, 0, 1))
    n_pos = K.sum(y_pos)
    y_neg = 1 - y_pos
    n_neg = K.sum(y_neg)
    n = n_pos + n_neg
    y_pred_pos = K.round(K.clip(y_pred, 0, 1))
    y_pred_neg = 1 - y_pred_pos
    tn = K.sum(K.round(K.clip(y_neg * y_pred_neg, 0, 1))) # TN
    return tn/n

def FP(y_true, y_pred):
    y_pos = K.round(K.clip(y_true, 0, 1))
    n_pos = K.sum(y_pos)
    y_neg = 1 - y_pos
    n_neg = K.sum(y_neg)
    n = n_pos + n_neg
    tn = K.sum(K.round(K.clip(y_neg * y_pred, 0, 1))) # FP
    return tn/n

def FN(y_true, y_pred):
    y_pos = K.round(K.clip(y_true, 0, 1))
    n_pos = K.sum(y_pos)
    y_neg = 1 - y_pos
    n_neg = K.sum(y_neg)
    n = n_pos + n_neg
    y_pred_pos = K.round(K.clip(y_pred, 0, 1))
    y_pred_neg = 1 - y_pred_pos
    tn = K.sum(K.round(K.clip(y_true * y_pred_neg, 0, 1))) # FN
    return tn/n

print('Build model...')

model = Sequential()
model.add(Embedding(max_features, embedding_size, input_length=maxlen))
#model.add(Dropout(0.3)) # should this be changed?? 0.2/0.3/0.5

if args.model_type == "CNN":
    model.add(Convolution1D(filters=nb_filter,
                            kernel_size=10,
                            padding='valid',
                            activation='relu'))
    model.add(MaxPooling1D(pool_size=pool_length))
    model.add(Convolution1D(filters=nb_filter,
                            kernel_size=5,
                            padding='valid',
                            activation='relu'))
    model.add(MaxPooling1D(pool_size=pool_length))
    model.add(Flatten())
else:
    if args.model_type == "BiLSTM":
        model.add(Bidirectional(LSTM(bilstm_output_size, return_sequences=True)))
        model.add(Bidirectional(LSTM(bilstm_output_size)))
    else:
        model.add(Convolution1D(filters=nb_filter,
                                kernel_size=10,
                                padding='valid',
                                activation='relu'))
        model.add(MaxPooling1D(pool_size=pool_length))
        model.add(Convolution1D(filters=nb_filter,
                                kernel_size=5,
                                padding='valid',
                                activation='relu'))
        model.add(MaxPooling1D(pool_size=pool_length))
        model.add(Bidirectional(LSTM(bilstm_output_size, return_sequences=True)))
        model.add(Bidirectional(LSTM(bilstm_output_size)))        

model.add(Dense(1))   
model.add(Activation('sigmoid'))

if args.optimizer:
    model.compile(loss='binary_crossentropy',
                optimizer=optimizers.Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0),
                metrics=['accuracy'])#,f1_m,precision_m, recall_m, TP, TN, FP, FN])
else:
    model.compile(loss='binary_crossentropy',
                metrics=['accuracy'])#,f1_m,precision_m, recall_m, TP, TN, FP, FN])

model.summary()
        
###############################################
############# Train the model #################
###############################################

initial_learning_rate = args.LR
decay = initial_learning_rate / epochs
def lr_time_based_decay(epoch, lr):
    return lr * 1 / (1 + decay * epoch)

print('Training...')
if not platform.node().startswith("ig"): # graphviz is not installed on the server. Avoid running this line
    plot_model(model, to_file='model.png')
history = History()
model_checkpoint = ModelCheckpoint(out_path+'temp_model.hdf5', monitor='loss', save_best_only=True)
tb_cb = keras.callbacks.TensorBoard(log_dir=out_path+'log', write_images=1, histogram_freq=0)
es_cb = keras.callbacks.EarlyStopping(monitor='val_loss', patience=3)
cp_cb = keras.callbacks.ModelCheckpoint(filepath=out_path+'cp.ckpt',
                                                 save_weights_only=True,
                                                 verbose=1)

lrs = LearningRateScheduler(lr_time_based_decay, verbose=1)
if args.optimizer:
    if args.early_stopping:
        callbacks = [history, model_checkpoint, tb_cb, es_cb, cp_cb]
    else:
        callbacks = [history, model_checkpoint, tb_cb, cp_cb] #es_cb,
else:
    if args.early_stopping:
        callbacks = [history, model_checkpoint, tb_cb, es_cb, lrs, cp_cb]
    else:
        callbacks = [history, model_checkpoint, tb_cb, lrs, cp_cb]
# balance data in the each batch, we have overfit to false. the function has an option for that
# pip install keras-balanced-batch-generator

if args.generator:
    y = to_categorical(y_train)
    training_generator, steps_per_epoch = balanced_batch_generator(X_train, y, sampler=RandomOverSampler(sampling_strategy='minority'), batch_size=batch_size, random_state=42)
    history = model.fit(training_generator, steps_per_epoch = steps_per_epoch,
                epochs=epochs, verbose=1, callbacks=callbacks,
                validation_data=(X_test, y_test))
else:
    if args.add_weights:
        # calc the weights to the classes
        class_weights = class_weight.compute_class_weight('balanced', classes = np.unique(y_train), y = y_train)
        # # change weights from sklearn to tensorflow input
        class_weights = {l:c for l,c in zip(np.unique(y_train), class_weights)}
        print("The weight for the classes are: ")
        print("\t- Class 0: "+str(class_weights[0]))
        print("\t- Class 1: "+str(class_weights[1]))
        with open(out_path+'params.txt', 'a') as the_file:
            the_file.write("The weight for the classes are: ")
            the_file.write("\t- Class 0: "+str(class_weights[0]))
            the_file.write("\t- Class 1: "+str(class_weights[1]))
        history = model.fit(X_train, y_train, batch_size=batch_size, class_weight=class_weights, 
        epochs=epochs, verbose=1, callbacks=callbacks, validation_data=(X_val, y_val), workers = 8)
    else:
        history = model.fit(X_train, y_train, batch_size=batch_size,
                epochs=epochs, verbose=1, callbacks=callbacks,
                validation_data=(X_val, y_val))

model.save(out_path+'fine_tune_model.h5')
model.save_weights(out_path+'fine_tune_model_weight')
history = history


#plt.plot()
fig, (ax1, ax2) = plt.subplots(2, sharex=True)
fig.suptitle(str(args.model_type)+" accuracy and loss")
# summarize history for acc
ax1.plot(history.history['accuracy'])
ax1.plot(history.history['val_accuracy'])
#ax1.title(args.model_type+' model accuracy')
ax1.set_ylabel('accuracy')
ax1.set_xlabel('epoch')
ax1.legend(['train', 'validation'], loc='upper left')
#plt.show()
# summarize history for loss
ax2.plot(history.history['loss'])
ax2.plot(history.history['val_loss'])
#ax2.title(args.model_type+' model loss')
ax2.set_ylabel('loss')
ax2.set_xlabel('epoch')
ax2.legend(['train', 'validation'], loc='upper left')
#plt.show()
fig.savefig(out_path+'acc_loss.png')
plt.close(fig)
accy = history.history['accuracy']
lossy = history.history['loss']
np_accy = np.array(accy)
np_lossy = np.array(lossy)
np.savetxt(out_path+'save_acc.txt', np_accy)
np.savetxt(out_path+'save_loss.txt', np_accy)
accy = history.history['val_accuracy']
lossy = history.history['val_loss']
np_accy = np.array(accy)
np_lossy = np.array(lossy)
np.savetxt(out_path+'save_val_accuracy.txt', np_accy)
np.savetxt(out_path+'save_val_loss.txt', np_accy)
model.save(out_path+args.model_type+'_model.h5')
score, acc, *is_anything_else_being_returned = model.evaluate(X_test, y_test, batch_size=batch_size)
np_acc = np.array(acc, ndmin=1)
np_loss = np.array(score, ndmin=1)
np.savetxt(out_path+'save_test_accuracy.txt', np_acc)
np.savetxt(out_path+'save_test_loss.txt', np_loss)
print('Test score:', score)
print('Test accuracy:', acc)
y_pred = model.predict(X_test)
confusion_matrix = sklearn.metrics.confusion_matrix(y_test, np.rint(y_pred), normalize = "all")
fig, ax = plot_confusion_matrix(conf_mat = confusion_matrix,
                      class_names = ['Positive', 'Negative'],
                      show_absolute = False,
                      show_normed = True,
                      colorbar = True)
fig.savefig(out_path+'cm_test.png')
print(confusion_matrix)
print('############################################')