from __future__ import print_function
import numpy as np
import numpy as np
import pickle
import processing as pg
import os.path
import h5py
import sklearn  
import keras
from keras.models import model_from_json, load_model, Sequential, Model
from keras.preprocessing import sequence
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM, GRU, SimpleRNN
from keras.layers.convolutional import Convolution1D, MaxPooling1D
from keras.layers import Dense, LSTM, Lambda, TimeDistributed, Input, Masking, Bidirectional, Flatten
from keras.datasets import imdb
from keras.utils import np_utils
from keras.utils.vis_utils import plot_model
from keras.callbacks import History, ModelCheckpoint, LearningRateScheduler
from keras import backend as K
from matplotlib import pyplot as plt
plt.switch_backend('agg')
from tabulate import tabulate
from keras_balanced_batch_generator import make_generator
from tensorflow.keras.utils import to_categorical
from tensorflow.keras import optimizers
from imblearn.tensorflow import balanced_batch_generator
from imblearn.under_sampling import NearMiss
from imblearn.over_sampling import RandomOverSampler 
import platform
import argparse
from sklearn.utils import class_weight
from mlxtend.plotting import plot_confusion_matrix
from sklearn.model_selection import train_test_split
import os
import pandas as pd
 
#########################################################################
#### convert all the cdr3_aa of each repertoire to a single sequence ####
#########################################################################

path_rep = "celiac_df2.csv"
df = pd.read_csv(path_rep, sep=",", usecols=["subject", "cdr3_aa", "Subjects_Health.Status"])
df['Subjects_Health.Status'] = df['Subjects_Health.Status'].replace(['Healthy','Celiac'],[0,1])

#### cut the cdr3_aa sequences
maxlen_pad = 21 #df['cdr3_aa'].str.len().quantile(0.9) #21

def encode(s1):
    a = []
    dic = {'0':0,'A':1,'C':2,'D':3,'E':4,'F':5,'G':6,'H':7,'I':8,'K':9,
           'L':10,'M':11,'N':12,'P':13,'Q':14,'R':15,'S':16,'T':17,
           'V':18,'W':19,'Y':20,'X':21,'!':22}
    for i in range(len(s1)):
        a.append(dic.get(s1[i]))
    return a

subjects = df.subject.unique()
X = []
for i in range(len(subjects)):  
    temp = df[(df.subject == subjects[i])]
    cdr3_aa_rep_vec = []
    for cdr3_aa_rep in temp.cdr3_aa:
        cdr3_aa_rep = cdr3_aa_rep[0:maxlen_pad]
        pad = maxlen_pad - len(cdr3_aa_rep)
        pad = "0"*pad
        cdr3_aa_rep += pad
        cdr3_aa_rep += '!!!!!'
        cdr3_aa_rep_vec.append(cdr3_aa_rep)
    cdr3_aa = ''.join(cdr3_aa_rep_vec)
    cdr3_aa = encode(cdr3_aa)
    # cdr3_aa = cdr3_aa[0:100000]
    X.append(cdr3_aa)

maxlen = max(len(j) for j in X) # max number of AA sequence
X = sequence.pad_sequences(X, maxlen=maxlen, padding="post")

################################################################
#### split the celiac dataset to train, validation and test ####
################################################################

df_sub = df[['subject','Subjects_Health.Status']].drop_duplicates()

X_train2, X_test2, y_train2, y_test2 = train_test_split(range(len(df_sub['subject'])), df_sub['Subjects_Health.Status'], test_size=0.2, shuffle=True, random_state=113)  #args.split_threshold
X_train2, X_val2, y_train2, y_val2 = train_test_split(X_train2, y_train2, test_size=0.25, random_state=113)                 

X_train = np.asarray([np.asarray(X[i]) for i in X_train2])
X_val = np.asarray([np.asarray(X[i]) for i in X_val2])
X_test = np.asarray([np.asarray(X[i]) for i in X_test2])
y_train = y_train2
y_val = y_val2
y_test = y_test2

#######################################################
#### load the weights of the best CNN-BiLSTM model ####
#######################################################

out_path = '/home/bcrlab/peresay/NN project/LR_opt_S20_EZ_32_weights_MT_CNN_BiLSTM_no_dr/'
transfer_path = '/home/bcrlab/peresay/NN project/transfer/'

# reproducibility
seed = 1258
np.random.seed(seed)

# Embedding
max_features = 23 # number of AA + '!' string
embedding_size = 32

# Convolution
nb_filter = 32 
pool_length = 2

# BiLSTM
bilstm_output_size = 70

# The CNN-BiLSTM model
model = Sequential()
model.add(Embedding(max_features, embedding_size, input_length=maxlen))
model.add(Convolution1D(filters=nb_filter,
                                    kernel_size=10,
                                    padding='valid',
                                    activation='relu'))
model.add(MaxPooling1D(pool_size=pool_length))
model.add(Convolution1D(filters=nb_filter,
                                    kernel_size=5,
                                    padding='valid',
                                    activation='relu'))
model.add(MaxPooling1D(pool_size=pool_length))
model.add(Bidirectional(LSTM(bilstm_output_size, return_sequences=True)))
model.add(Bidirectional(LSTM(bilstm_output_size)))   

# for layer in model.layers:
#     print(layer.name)

model.get_layer('embedding').trainable = False
model.get_layer('conv1d').trainable = False
model.get_layer('max_pooling1d').trainable = False
model.get_layer('conv1d_1').trainable = False
model.get_layer('max_pooling1d_1').trainable = False
model.get_layer('bidirectional').trainable = False
model.get_layer('bidirectional_1').trainable = False

# Restore the weights
model.load_weights(out_path+'cp.ckpt') 

# define the MLP model
model.add(Dense(50, input_dim=140, activation='relu')) # 140 is the dimension of the output from the previous layer
model.add(Dense(20, activation='relu'))
model.add(Dense(1, activation='sigmoid'))

# compile the model
model.compile(loss='binary_crossentropy',
                optimizer=optimizers.Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0),
                metrics=['accuracy'])#,f1_m,precision_m, recall_m, TP, TN, FP, FN])

# fit the model on the dataset
history = model.fit(X_train, y_train, epochs=20, batch_size=32, verbose=1, validation_data=(X_val, y_val))

# save the model weights that was trained on the training data
model.save_weights(out_path+'pretrained_model_weight') 

# load the model weights
# model.load_weights(out_path+'pretrained_model_weight')

############# add figures #############

fig, (ax1, ax2) = plt.subplots(2, sharex=True)
plt.xticks(range(1,21))
fig.suptitle(" accuracy and loss")
# summarize history for acc
ax1.plot(range(1,21),history.history['accuracy'])
ax1.plot(range(1,21),history.history['val_accuracy'])
#ax1.title(args.model_type+' model accuracy')
ax1.set_ylabel('accuracy')
ax1.set_xlabel('epoch')
ax1.legend(['train', 'validation'], loc='upper left')
#plt.show()
# summarize history for loss
ax2.plot(range(1,21),history.history['loss'])
ax2.plot(range(1,21),history.history['val_loss'])
#ax2.title(args.model_type+' model loss')
ax2.set_ylabel('loss')
ax2.set_xlabel('epoch')
ax2.legend(['train', 'validation'], loc='upper left')
#plt.show()
fig.savefig(transfer_path+'acc_loss_transfer.png')
plt.close(fig)
accy = history.history['accuracy']
lossy = history.history['loss']
np_accy = np.array(accy)
np_lossy = np.array(lossy)
np.savetxt(transfer_path+'save_acc_transfer.txt', np_accy)
np.savetxt(transfer_path+'save_loss_transfer.txt', np_accy)
accy = history.history['val_accuracy']
lossy = history.history['val_loss']
np_accy = np.array(accy)
np_lossy = np.array(lossy)
np.savetxt(transfer_path+'save_val_accuracy_transfer.txt', np_accy)
np.savetxt(transfer_path+'save_val_loss_transfer.txt', np_accy)
model.save(transfer_path+'model_transfer.h5')

#######

# Evaluate the model on the test data
# loss_test, acc_test = model.evaluate(X_test, y_test, verbose=2) ##
# print("Restored model, accuracy: {:5.2f}%".format(100 * acc_test))

batch_size = 20
score, acc, *is_anything_else_being_returned = model.evaluate(X_test, y_test, batch_size=batch_size)
np_acc = np.array(acc, ndmin=1)
np_loss = np.array(score, ndmin=1)
np.savetxt(transfer_path+'save_test_accuracy_transfer.txt', np_acc)
np.savetxt(transfer_path+'save_test_loss_transfer.txt', np_loss)
print('Test transfer score:', score)
print('Test transfer accuracy:', acc)
y_pred = model.predict(X_test)
confusion_matrix = sklearn.metrics.confusion_matrix(y_test, np.rint(y_pred), normalize = "all")
fig, ax = plot_confusion_matrix(conf_mat = confusion_matrix,
                      class_names = ['Positive', 'Negative'],
                      show_absolute = False,
                      show_normed = True,
                      colorbar = True)
fig.savefig(transfer_path+'cm_test_transfer.png')
print(confusion_matrix)
print('############################################')