# CeD classification network #

In this repository we present a CeD classification network by using transfer learning. At first we applied aCNN-BiLSTM network on TG2 and gliadin dataset.
Then we fixed this network weights and add a MLP network to classify the complete CeD repertoire dataset.

